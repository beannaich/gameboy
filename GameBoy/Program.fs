module GameBoy.Program

open System.IO
open GameBoy.APU
open GameBoy.Boards
open GameBoy.CPU
open GameBoy.Platform.Audio
open GameBoy.Platform.Video
open GameBoy.Platform.Util
open GameBoy.PPU


let create binary audio video =
  let board =
    Cartridge.create(binary)
      |> Result.bind Board.create
      |> Result.get

  let state = State.init "gb/boot.rom"

  let sendIRQ interrupt =
    Cpu.irq interrupt state.cpu

  let resetDivider () =
    Tma.resetDivider sendIRQ state.tma

  let memoryReader = MemoryMap.read board state
  let memoryWriter = MemoryMap.write resetDivider board state

  let updateAPU = Apu.clock memoryReader
  let updateCPU = Cpu.update memoryReader memoryWriter
  let updatePPU = Ppu.clock sendIRQ memoryReader memoryWriter
  let updateTMA = Tma.clock sendIRQ

  let mutable cycles = 0

  let runForOneFrame () =
    let cyclesPerSecond = 4194304

    state.pad |> Pad.frame

    while cycles < cyclesPerSecond do
      let adding = state.cpu |> updateCPU

      state.apu |> updateAPU audio adding
      state.ppu |> updatePPU video adding
      state.tma |> updateTMA adding

      cycles <- cycles + (adding * 60)

    cycles <- cycles - cyclesPerSecond

  runForOneFrame


let runGame fileName =
  let audio =
    AudioBackend.create
      { backend = "sdl2"
        channels = 2
        sampleRate = 48000 }

  let video =
    VideoBackend.create
      { backend = "sdl2"
        width = 160
        height = 144 }

  let audioSink = AudioBackend.getSink audio
  let videoSink = VideoBackend.getSink video
  let driver = create (File.ReadAllBytes fileName) audioSink videoSink

  let rec loop status =
    match status with
    | VideoStatus.Exit ->
      ()

    | VideoStatus.Continue ->
      driver()
      AudioBackend.render(audio)
      VideoBackend.render(video) |> loop

  loop(VideoStatus.Continue)


[<EntryPoint>]
let main args =
  args
    |> Array.tryItem 0
    |> Option.iter runGame
  0
