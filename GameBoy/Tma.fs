namespace GameBoy

type Tma =
  { mutable divider: int
    mutable counter: int
    mutable control: int
    mutable modulus: int }

module Tma =

  open GameBoy.CPU


  let private log2 x = log(x) / log(2.0)


  let private freq x = int <| log2(4194304.0 / x)


  let lut = [|
    freq   4_096.0
    freq 242_144.0
    freq  65_536.0
    freq  16_384.0
  |]


  let init () =
    { divider = 0
      counter = 0
      control = 0
      modulus = 0 }


  let tickCounter irq tma =
    tma.counter <- (tma.counter + 1) &&& 0xff

    if tma.counter = 0 then
      tma.counter <- tma.modulus
      irq Interrupt.Elapse


  let writeDivider irq tma next =
    let prev = tma.divider

    if (tma.control &&& 4) <> 0 then
      let bit = lut.[tma.control &&& 3]
      let prevBit = (prev >>> bit) &&& 1
      let nextBit = (next >>> bit) &&& 1

      if prevBit = 1 && nextBit = 0 then
        tickCounter irq tma

    tma.divider <- next


  let resetDivider irq tma =
    writeDivider irq tma 0


  let tick irq tma =
    writeDivider irq tma (tma.divider + 1)


  let clock irq amount tma =
    for _ in 1 .. amount do
      tick irq tma
