namespace GameBoy.APU

type NoiState =
  { mutable enabled: bool
    mutable period: int
    mutable timer: int

    duration: Duration
    envelope: Envelope

    regs: byte []

    mutable dacPower: bool
    mutable lfsr: int
    mutable lfsrMode: int

    mutable lfsrFrequency: int
    mutable lfsrDivisor: int }

module NoiState =

  open GameBoy


  let init () =
    { enabled = false
      period = 0
      timer = 0

      duration = Duration.init ()
      envelope = Envelope.init ()

      regs = Array.zeroCreate<byte> 5

      dacPower = false
      lfsr = 0x7fff
      lfsrMode = 0

      lfsrFrequency = 0
      lfsrDivisor = 0 }


  let divisorLut = [|
    8
    16
    32
    48
    64
    80
    96
    112
  |]


  let read noi = Reader (fun address ->
    match address with
    | 0xff1fus -> 0xffuy ||| noi.regs.[0]
    | 0xff20us -> 0xffuy ||| noi.regs.[1]
    | 0xff21us -> 0x00uy ||| noi.regs.[2]
    | 0xff22us -> 0x00uy ||| noi.regs.[3]
    | 0xff23us -> 0xbfuy ||| noi.regs.[4]
    | _ ->
      failwith ""
  )


  let write noi = Writer (fun address data ->
    let index = (int address) - 0xff1f
    noi.regs.[index] <- data

    match address with
    | 0xff1fus -> ()
    | 0xff20us ->
      noi.duration.counter <- 64 - ((int data) &&& 63)

    | 0xff21us ->
      noi.dacPower <- (data &&& 0xf8uy) <> 0uy
      if not noi.dacPower then
        noi.enabled <- false

      noi.envelope.latch <- ((int data) >>> 4) &&& 15
      noi.envelope.direction <- ((int data) >>> 3) &&& 1
      noi.envelope.period <- ((int data) >>> 0) &&& 7

    | 0xff22us ->
      noi.lfsrFrequency <- ((int data) >>> 4) &&& 15
      noi.lfsrMode <- ((int data) >>> 3) &&& 1
      noi.lfsrDivisor <- ((int data) >>> 0) &&& 7

      noi.period <- divisorLut.[noi.lfsrDivisor] <<< noi.lfsrFrequency

    | 0xff23us ->
      noi.duration.enabled <- (data &&& 0x40uy) <> 0uy

      if (data &&& 0x80uy) <> 0uy && noi.dacPower then
        noi.timer <- noi.period
        noi.envelope.counter <- noi.envelope.latch
        noi.envelope.timer <- noi.envelope.period
        noi.lfsr <- 0x7fff
        noi.enabled <- true

        if noi.duration.counter = 0 then
          noi.duration.counter <- 64

    | _ ->
      failwith ""
  )


  let sample noi =
    if noi.enabled then
      noi.envelope.counter * ((~~~noi.lfsr) &&& 1)
    else
      0
