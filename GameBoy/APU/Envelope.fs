namespace GameBoy.APU

type Envelope =
  { mutable counter: int
    mutable direction: int
    mutable latch: int
    mutable period: int
    mutable timer: int }

module Envelope =

  let init () =
    { counter = 0
      direction = 0
      latch = 0
      period = 0
      timer = 0 }


  let tick envelope =
    if envelope.timer <> 0 && envelope.period <> 0 then
      envelope.timer <- envelope.timer - 1

      if envelope.timer = 0 then
        envelope.timer <- envelope.period

        envelope.counter <-
          match envelope.direction with
          | 0 -> max 0x0 (envelope.counter - 1)
          | _ -> min 0xf (envelope.counter + 1)
