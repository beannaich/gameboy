namespace GameBoy.APU

type Duty =
  { mutable form: int
    mutable step: int }

module Duty =

  let init () =
    { form = 0
      step = 0 }


  let flag duty =
    match duty.form with
    | 0 -> ((duty.step - 1) &&& 7) <  1
    | 1 -> ((duty.step - 1) &&& 7) <  2
    | 2 -> ((duty.step - 1) &&& 7) <  4
    | _ -> ((duty.step - 1) &&& 7) >= 2


  let tick duty =
    duty.step <- (duty.step + 1) &&& 7
