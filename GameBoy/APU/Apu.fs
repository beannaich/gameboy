namespace GameBoy.APU

type Apu =
  { mutable sampleTimer: int
    samplePeriod: int

    mutable sequenceTimer: int
    mutable sequenceStep: int

    mutable enabled: bool
    mutable outputVinL: int
    mutable outputVinR: int
    speakerSelect: int[]
    speakerVolume: int[]

    sq1: Sq1State
    sq2: Sq2State
    wav: WavState
    noi: NoiState }

module Apu =

  open GameBoy
  open GameBoy.Platform.Audio


  let init () =
    { sampleTimer = 0
      samplePeriod = 4194304

      sequenceTimer = 4194304 / 2048
      sequenceStep = 0

      enabled = false
      outputVinL = 0
      outputVinR = 0
      speakerSelect = Array.zeroCreate<_> 2
      speakerVolume = Array.zeroCreate<_> 2

      sq1 = Sq1State.init ()
      sq2 = Sq2State.init ()
      wav = WavState.init ()
      noi = NoiState.init () }


  let read apu = Reader (fun address ->
    match address with
    | n when n >= 0xff10us && n <= 0xff14us -> Reader.run (Sq1State.read apu.sq1) address
    | n when n >= 0xff15us && n <= 0xff19us -> Reader.run (Sq2State.read apu.sq2) address
    | n when n >= 0xff1aus && n <= 0xff1eus -> Reader.run (WavState.read apu.wav) address
    | n when n >= 0xff1fus && n <= 0xff23us -> Reader.run (NoiState.read apu.noi) address
    | 0xff24us ->
      byte (
        (apu.outputVinL        <<< 7) |||
        (apu.speakerVolume.[0] <<< 4) |||
        (apu.outputVinR        <<< 3) |||
        (apu.speakerVolume.[1] <<< 0)
      )

    | 0xff25us ->
      byte (
        (apu.speakerSelect.[0] <<< 4) |||
        (apu.speakerSelect.[1] <<< 0)
      )

    | 0xff26us ->
      (if apu.enabled     then 0x80uy else 0uy) |||
      (if apu.noi.enabled then 0x08uy else 0uy) |||
      (if apu.wav.enabled then 0x04uy else 0uy) |||
      (if apu.sq2.enabled then 0x02uy else 0uy) |||
      (if apu.sq1.enabled then 0x01uy else 0uy) ||| 0x70uy

    | 0xff27us -> 0xffuy
    | 0xff28us -> 0xffuy
    | 0xff29us -> 0xffuy
    | 0xff2aus -> 0xffuy
    | 0xff2bus -> 0xffuy
    | 0xff2cus -> 0xffuy
    | 0xff2dus -> 0xffuy
    | 0xff2eus -> 0xffuy
    | 0xff2fus -> 0xffuy
    | _ ->
      failwith ""
  )


  let write apu = Writer (fun address data ->
    match address with
    | n when n >= 0xff10us && n <= 0xff14us -> Writer.run (Sq1State.write apu.sq1) address data
    | n when n >= 0xff15us && n <= 0xff19us -> Writer.run (Sq2State.write apu.sq2) address data
    | n when n >= 0xff1aus && n <= 0xff1eus -> Writer.run (WavState.write apu.wav) address data
    | n when n >= 0xff1fus && n <= 0xff23us -> Writer.run (NoiState.write apu.noi) address data
    | 0xff24us ->
      apu.outputVinL        <- ((int data) >>> 7) &&& 1
      apu.speakerVolume.[0] <- ((int data) >>> 4) &&& 7
      apu.outputVinR        <- ((int data) >>> 3) &&& 1
      apu.speakerVolume.[1] <- ((int data) >>> 0) &&& 7

    | 0xff25us ->
      apu.speakerSelect.[0] <- ((int data) >>> 4) &&& 15
      apu.speakerSelect.[1] <- ((int data) >>> 0) &&& 15

    | 0xff26us ->
      apu.enabled <- ((int data) &&& 0x80) <> 0

    | _ ->
      failwith ""
  )



  let durationTick apu =
    if Duration.tick(apu.sq1.duration) then apu.sq1.enabled <- false
    if Duration.tick(apu.sq2.duration) then apu.sq2.enabled <- false
    if Duration.tick(apu.wav.duration) then apu.wav.enabled <- false
    if Duration.tick(apu.noi.duration) then apu.noi.enabled <- false


  let envelopeTick apu =
    Envelope.tick(apu.sq1.envelope)
    Envelope.tick(apu.sq2.envelope)
    Envelope.tick(apu.noi.envelope)


  let renderSample audio (state: Apu) =
    let sq1Out = Sq1State.sample state.sq1
    let sq2Out = Sq2State.sample state.sq2
    let wavOut = WavState.sample state.wav
    let noiOut = NoiState.sample state.noi

    for speaker in 0 .. 1 do
      let mutable sample = 0
      let select = state.speakerSelect.[speaker]
      let volume = state.speakerVolume.[speaker]

      if (select &&& 1) <> 0 then sample <- sample + sq1Out
      if (select &&& 2) <> 0 then sample <- sample + sq2Out
      if (select &&& 4) <> 0 then sample <- sample + wavOut
      if (select &&& 8) <> 0 then sample <- sample + noiOut

      // apply volume correction

      AudioSink.run audio ((sample * volume * 32768) / 16 / 4 / 8)


  let tick apu read audio =
    apu.sampleTimer <- apu.sampleTimer - 48000
    if apu.sampleTimer <= 0 then
      apu.sampleTimer <- apu.sampleTimer + apu.samplePeriod
      renderSample audio apu

    let noi = apu.noi
    let sq1 = apu.sq1
    let sq2 = apu.sq2
    let wav = apu.wav

    if apu.sequenceTimer <> 0 then
      apu.sequenceTimer <- apu.sequenceTimer - 1
      if apu.sequenceTimer = 0 then
        apu.sequenceTimer <- 4194304 / 512

        match apu.sequenceStep with
        | 0 ->
          durationTick apu

        | 1 ->
          ()

        | 2 ->
          durationTick apu
          Sweep.tick(sq1)

        | 3 ->
          ()

        | 4 ->
          durationTick apu

        | 5 ->
          ()

        | 6 ->
          durationTick apu
          Sweep.tick(sq1)

        | 7 ->
          envelopeTick apu

        | _ ->
          failwith ""

        apu.sequenceStep <- (apu.sequenceStep + 1) &&& 7

    if sq1.enabled && sq1.timer <> 0 then
      sq1.timer <- sq1.timer - 1
      if sq1.timer = 0 then
        sq1.timer <- (2048 - sq1.period) * 4
        Duty.tick(sq1.duty)

    if sq2.enabled && sq2.timer <> 0 then
      sq2.timer <- sq2.timer - 1
      if sq2.timer = 0 then
        sq2.timer <- (2048 - sq2.period) * 4
        Duty.tick(sq2.duty)

    if wav.enabled && wav.timer <> 0 then
      wav.timer <- wav.timer - 1
      if wav.timer = 0 then
        wav.timer <- (2048 - wav.period) * 2
        wav.waveRamShift <- wav.waveRamShift ^^^ 4

        if wav.waveRamShift = 0 then
          wav.waveRamCursor <- (wav.waveRamCursor + 1) &&& 15

          let address = uint16 (0xff30 ||| wav.waveRamCursor)
          wav.waveRamSample <- read(address)

        wav.waveRamOutput <- ((int wav.waveRamSample) >>> wav.waveRamShift) &&& 15

    if noi.enabled && noi.timer <> 0 then
      noi.timer <- noi.timer - 1
      if noi.timer = 0 then
        noi.timer <- noi.period

        let tap0 = (noi.lfsr >>> 0) &&& 1
        let tap1 = (noi.lfsr >>> 1) &&& 1
        let next = (tap0 ^^^ tap1)

        noi.lfsr <- noi.lfsr >>> 1
        noi.lfsr <-
          if noi.lfsrMode = 1 then
            (noi.lfsr &&& (~~~0x4040)) ||| (next * 0x4040)
          else
            (noi.lfsr &&& (~~~0x4000)) ||| (next * 0x4000)


  let clock read audio amount apu =
    for _ in 1 .. amount do
      tick apu read audio
