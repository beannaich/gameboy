namespace GameBoy.APU

type Sq2State =
  { mutable enabled: bool
    mutable period: int
    mutable timer: int

    duration: Duration
    envelope: Envelope
    duty: Duty

    regs: byte []

    mutable dacPower: bool }

module Sq2State =

  open GameBoy


  let init () =
    { enabled = false
      period = 0
      timer = 0

      duration = Duration.init ()
      envelope = Envelope.init ()
      duty = Duty.init ()

      regs = Array.zeroCreate 5

      dacPower = false }


  let read sq2 = Reader (fun address ->
    match address with
    | 0xff15us -> 0xffuy ||| sq2.regs.[0]
    | 0xff16us -> 0x3fuy ||| sq2.regs.[1]
    | 0xff17us -> 0x00uy ||| sq2.regs.[2]
    | 0xff18us -> 0xffuy ||| sq2.regs.[3]
    | 0xff19us -> 0xbfuy ||| sq2.regs.[4]
    | _ ->
      failwith ""
  )


  let write sq2 = Writer (fun address data ->
    let index = (int address) - 0xff15
    sq2.regs.[index] <- byte data

    match address with
    | 0xff15us -> ()
    | 0xff16us ->
      sq2.duty.form <- ((int data) >>> 6) &&& 3
      sq2.duration.counter <- 64 - ((int data) &&& 63)

    | 0xff17us ->
      sq2.dacPower <- ((int data) &&& 0xf8) <> 0
      if not sq2.dacPower then
        sq2.enabled <- false

      sq2.envelope.latch <- ((int data) >>> 4) &&& 15
      sq2.envelope.direction <- ((int data) >>> 3) &&& 1
      sq2.envelope.period <- ((int data) >>> 0) &&& 7

    | 0xff18us ->
      sq2.period <- (sq2.period &&& 0x700) ||| (((int data) <<< 0) &&& 0x0ff)

    | 0xff19us ->
      sq2.period <- (sq2.period &&& 0x0ff) ||| (((int data) <<< 8) &&& 0x700)
      sq2.duration.enabled <- ((int data) &&& 0x40) <> 0

      if ((int data) &&& 0x80) <> 0 && sq2.dacPower then
        sq2.timer <- (0x800 - sq2.period) * 4
        sq2.envelope.counter <- sq2.envelope.latch
        sq2.envelope.timer <- sq2.envelope.period
        sq2.enabled <- true

        if sq2.duration.counter = 0 then
          sq2.duration.counter <- 64

    | _ ->
      failwith ""
  )


  let sample sq2 =
    if sq2.enabled && Duty.flag(sq2.duty) then
      sq2.envelope.counter
    else
      0
