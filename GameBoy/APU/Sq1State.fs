namespace GameBoy.APU

type Sq1State =
  { mutable enabled: bool
    mutable period: int
    mutable timer: int

    duration: Duration
    envelope: Envelope
    duty: Duty

    regs: byte []

    mutable dacPower: bool

    mutable sweepEnabled: bool
    mutable sweepDirection: int
    mutable sweepPeriod: int
    mutable sweepTimer: int
    mutable sweepShift: int }

module Sq1State =

  open GameBoy


  let init () =
    { enabled = false
      period = 0
      timer = 0

      duration = Duration.init ()
      envelope = Envelope.init ()
      duty = Duty.init ()

      regs = Array.zeroCreate 5

      dacPower = false

      sweepEnabled = false
      sweepDirection = 0
      sweepPeriod = 0
      sweepTimer = 0
      sweepShift = 0 }


  let read sq1 = Reader (fun address ->
    match address with
    | 0xff10us -> 0x80uy ||| (uint8 sq1.regs.[0])
    | 0xff11us -> 0x3fuy ||| (uint8 sq1.regs.[1])
    | 0xff12us -> 0x00uy ||| (uint8 sq1.regs.[2])
    | 0xff13us -> 0xffuy ||| (uint8 sq1.regs.[3])
    | 0xff14us -> 0xbfuy ||| (uint8 sq1.regs.[4])
    | _ ->
      failwith ""
  )


  let write sq1 = Writer (fun address data ->
    let index = (int address) - 0xff10
    sq1.regs.[index] <- byte data

    match address with
    | 0xff10us ->
      sq1.sweepPeriod <- ((int data) >>> 4) &&& 7
      sq1.sweepDirection <- ((int data) >>> 3) &&& 1
      sq1.sweepShift <- ((int data) >>> 0) &&& 7

    | 0xff11us ->
      sq1.duty.form <- ((int data) >>> 6) &&& 3
      sq1.duration.counter <- 64 - ((int data) &&& 63)

    | 0xff12us ->
      sq1.dacPower <- ((int data) &&& 0xf8) <> 0
      if not sq1.dacPower then
        sq1.enabled <- false

      sq1.envelope.latch <- ((int data) >>> 4) &&& 15
      sq1.envelope.direction <- ((int data) >>> 3) &&& 1
      sq1.envelope.period <- ((int data) >>> 0) &&& 7

    | 0xff13us ->
      sq1.period <- (sq1.period &&& 0x700) ||| (((int data) <<< 0) &&& 0x0ff)

    | 0xff14us ->
      sq1.period <- (sq1.period &&& 0x0ff) ||| (((int data) <<< 8) &&& 0x700)
      sq1.duration.enabled <- ((int data) &&& 0x40) <> 0

      if ((int data) &&& 0x80) <> 0 && sq1.dacPower then
        sq1.timer <- (0x800 - sq1.period) * 4
        sq1.envelope.counter <- sq1.envelope.latch
        sq1.envelope.timer <- sq1.envelope.period
        sq1.enabled <- true

        if sq1.duration.counter = 0 then
          sq1.duration.counter <- 64

        sq1.sweepTimer <- sq1.sweepPeriod
        sq1.sweepEnabled <- sq1.sweepPeriod <> 0 || sq1.sweepShift <> 0

    | _ ->
      failwith ""
  )


  let sample sq1 =
    if sq1.enabled && Duty.flag(sq1.duty) then
      sq1.envelope.counter
    else
      0
