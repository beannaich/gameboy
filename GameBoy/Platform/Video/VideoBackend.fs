namespace GameBoy.Platform.Video

[<RequireQualifiedAccess>]
type VideoBackend =
  | Null
  | SDL2 of SDL2Video

module VideoBackend =

  let create (definition: VideoDefinition) =
    match definition.backend with
    | "sdl2" ->
      VideoBackend.SDL2 <| SDL2Video.create(definition)

    | _ ->
      VideoBackend.Null


  let destroy video =
    match video with
    | VideoBackend.Null ->
      ()

    | VideoBackend.SDL2(sdl2) ->
      SDL2Video.destroy(sdl2)


  let getSink video =
    match video with
    | VideoBackend.Null ->
      VideoSink (fun _ _ _ -> ())

    | VideoBackend.SDL2(sdl2) ->
      SDL2Video.sink(sdl2)


  let render video =
    match video with
    | VideoBackend.Null ->
      VideoStatus.Continue

    | VideoBackend.SDL2(sdl2) ->
      SDL2Video.render(sdl2)
