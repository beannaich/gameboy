namespace GameBoy.Platform.Video

type VideoDefinition =
  { backend: string
    width: int
    height: int }
