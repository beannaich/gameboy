namespace GameBoy.Platform.Audio

type AudioDefinition =
  { backend: string
    channels: int
    sampleRate: int }
