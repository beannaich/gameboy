namespace GameBoy.Boards

open System

type MBC3 =
  { mutable rtc: DateTime
    mutable ramEnable: bool
    mutable ramPage: int
    mutable romPage: int
    mutable rtcLatch: int }

module MBC3 =

  open GameBoy
  open GameBoy.Platform


  let init () =
    { rtc = DateTime.Now
      ramEnable = false
      ramPage = 0
      romPage = 1
      rtcLatch = 0 }


  let private read0000 cartridge mbc3 = Reader (fun address ->
    cartridge
      |> Cartridge.readRom ((int address) &&& 0x3fff)
  )


  let private read4000 cartridge mbc3 = Reader (fun address ->
    cartridge
      |> Cartridge.readRom (((int address) &&& 0x3fff) ||| (mbc3.romPage <<< 14))
  )


  let private readA000 cartridge mbc3 = Reader (fun address ->
      if not mbc3.ramEnable then
        0x00uy
      else
        match mbc3.ramPage with
        | 0x0 -> cartridge |> Cartridge.readRam (((int address) &&& 0x1fff) ||| 0x0000)
        | 0x1 -> cartridge |> Cartridge.readRam (((int address) &&& 0x1fff) ||| 0x2000)
        | 0x2 -> cartridge |> Cartridge.readRam (((int address) &&& 0x1fff) ||| 0x4000)
        | 0x3 -> cartridge |> Cartridge.readRam (((int address) &&& 0x1fff) ||| 0x6000)

        // RTC Registers
        | 0x8 -> byte mbc3.rtc.Second
        | 0x9 -> byte mbc3.rtc.Minute
        | 0xa -> byte mbc3.rtc.Hour
        | 0xb -> 0uy
        | 0xc -> 0uy
        | _   -> 0uy
  )


  let private write0000 mbc3 = Writer (fun address data ->
    mbc3.ramEnable <- data = 0x0auy)


  let private write2000 mbc3 = Writer (fun address data ->
    mbc3.romPage <- ((int data) &&& 0x7f)

    if mbc3.romPage = 0 then
      mbc3.romPage <- 1
  )


  let private write4000 mbc3 = Writer (fun address data ->
    mbc3.ramPage <- int data)


  let private write6000 mbc3 = Writer (fun address data ->
    if mbc3.rtcLatch < ((int data) &&& 0x01) then
      mbc3.rtcLatch <- 0x01
      mbc3.rtc <- DateTime.Now
  )


  let private writeA000 cartridge mbc3 = Writer (fun address data ->
    if mbc3.ramEnable then
      match mbc3.ramPage with
      | 0x0 -> cartridge |> Cartridge.writeRam (((int address) &&& 0x1fff) ||| 0x0000) data
      | 0x1 -> cartridge |> Cartridge.writeRam (((int address) &&& 0x1fff) ||| 0x2000) data
      | 0x2 -> cartridge |> Cartridge.writeRam (((int address) &&& 0x1fff) ||| 0x4000) data
      | 0x3 -> cartridge |> Cartridge.writeRam (((int address) &&& 0x1fff) ||| 0x6000) data

      // RTC Registers
      | 0x8 -> ()
      | 0x9 -> ()
      | 0xa -> ()
      | 0xb -> ()
      | 0xc -> ()
      | _   -> ()
  )


  let read cartridge mbc3 = Reader (fun address ->
    match int address with
    | n when n >= 0x0000 && n <= 0x3fff -> Reader.run (read0000 cartridge mbc3) address
    | n when n >= 0x4000 && n <= 0x7fff -> Reader.run (read4000 cartridge mbc3) address
    | n when n >= 0xa000 && n <= 0xbfff -> Reader.run (readA000 cartridge mbc3) address
    | _ ->
      0xffuy
  )


  let write cartridge mbc3 = Writer (fun address data ->
    match int address with
    | n when n >= 0x0000 && n <= 0x1fff -> Writer.run (write0000 mbc3) address data
    | n when n >= 0x2000 && n <= 0x3fff -> Writer.run (write2000 mbc3) address data
    | n when n >= 0x4000 && n <= 0x5fff -> Writer.run (write4000 mbc3) address data
    | n when n >= 0x6000 && n <= 0x7fff -> Writer.run (write6000 mbc3) address data
    | n when n >= 0xa000 && n <= 0xbfff -> Writer.run (writeA000 cartridge mbc3) address data
    | _ ->
      ()
  )
