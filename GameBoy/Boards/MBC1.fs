namespace GameBoy.Boards

type MBC1 =
  { mutable romMode: bool
    mutable ramPage: int
    mutable romPage: int }

module MBC1 =

  open GameBoy


  let init () =
    { romMode = false
      ramPage = 0
      romPage = 1 }


  let private ramAddress address mbc1 =
    (address &&& 0x1fff) ||| mbc1.ramPage


  let private romAddress address mbc1 =
    if address < 0x4000 then
      address
    else
      if mbc1.romMode then
        (address &&& 0x3fff) ||| (mbc1.romPage <<< 14) ||| (mbc1.ramPage <<< 6)
      else
        (address &&& 0x3fff) ||| (mbc1.romPage <<< 14)


  let private read0000 cartridge mbc1 = Reader (fun address ->
    Cartridge.readRom (romAddress (int address) mbc1) cartridge
  )


  let private read4000 cartridge mbc1 = Reader (fun address ->
    Cartridge.readRom (romAddress (int address) mbc1) cartridge
  )


  let private readA000 cartridge mbc1 = Reader (fun address ->
    Cartridge.readRam (ramAddress (int address) mbc1) cartridge
  )


  let private write0000 mbc1 = Writer (fun address data ->
    ()
  )


  let private write2000 mbc1 = Writer (fun address data ->
    mbc1.romPage <- (int data) &&& 0x1f

    if mbc1.romPage = 0 then
      mbc1.romPage <- 1
  )


  let private write4000 mbc1 = Writer (fun address data ->
    mbc1.ramPage <- ((int data) &&& 0x03) <<< 13)


  let private write6000 mbc1 = Writer (fun address data ->
    mbc1.romMode <- ((int data) &&& 0x01) = 0)


  let private writeA000 cartridge mbc1 = Writer (fun address data ->
    Cartridge.writeRam (ramAddress (int address) mbc1) data cartridge
  )


  let read cartridge mbc1 = Reader (fun address ->
    match int address with
    | n when n >= 0x0000 && n <= 0x3fff -> Reader.run (read0000 cartridge mbc1) address
    | n when n >= 0x4000 && n <= 0x7fff -> Reader.run (read4000 cartridge mbc1) address
    | n when n >= 0xa000 && n <= 0xbfff -> Reader.run (readA000 cartridge mbc1) address
    | _ ->
      0xffuy)


  let write cartridge mbc1 = Writer (fun address data ->
    match int address with
    | n when n >= 0x0000 && n <= 0x1fff -> Writer.run (write0000 mbc1) address data
    | n when n >= 0x2000 && n <= 0x3fff -> Writer.run (write2000 mbc1) address data
    | n when n >= 0x4000 && n <= 0x5fff -> Writer.run (write4000 mbc1) address data
    | n when n >= 0x6000 && n <= 0x7fff -> Writer.run (write6000 mbc1) address data
    | n when n >= 0xa000 && n <= 0xbfff -> Writer.run (writeA000 cartridge mbc1) address data
    | _ ->
      ())
