namespace GameBoy.Boards

open GameBoy.Platform
open GameBoy.Platform.Util

type Cartridge =
  private
    { mbc: uint8
      rom: Memory
      ram: Memory option }

module Cartridge =

  let private mbcSpecifier (buffer: byte []) =
    Array.get buffer 0x147


  let private romSpecifier (buffer: byte []) =
    Array.get buffer 0x148


  let private ramSpecifier (buffer: byte []) =
    Array.get buffer 0x149


  let private ramSize =
    function
    | 0x00uy -> Ok <| None
    | 0x01uy -> Ok <| Some (BinaryUnit.kib 2)
    | 0x02uy -> Ok <| Some (BinaryUnit.kib 8)
    | 0x03uy -> Ok <| Some (BinaryUnit.kib 32)
    | 0x04uy -> Ok <| Some (BinaryUnit.kib 128)
    | 0x05uy -> Ok <| Some (BinaryUnit.kib 64)
    | _ ->
      Error ""


  let private romSize =
    function
    | 0x00uy -> Ok <| BinaryUnit.kib 32
    | 0x01uy -> Ok <| BinaryUnit.kib 64
    | 0x02uy -> Ok <| BinaryUnit.kib 128
    | 0x03uy -> Ok <| BinaryUnit.kib 256
    | 0x04uy -> Ok <| BinaryUnit.kib 512
    | 0x05uy -> Ok <| BinaryUnit.mib 1
    | 0x06uy -> Ok <| BinaryUnit.mib 2
    | 0x07uy -> Ok <| BinaryUnit.mib 4
    | 0x08uy -> Ok <| BinaryUnit.mib 8
    | 0x52uy -> Error "Multi-chip ROMs aren't supported yet." // 52h - 1.1MByte (72 banks)
    | 0x53uy -> Error "Multi-chip ROMs aren't supported yet." // 53h - 1.2MByte (80 banks)
    | 0x54uy -> Error "Multi-chip ROMs aren't supported yet." // 54h - 1.5MByte (96 banks)
    | _ ->
      Error ""


  let create buffer = result {
    let! romSize = romSize (romSpecifier buffer)
    let! ramSize = ramSize (ramSpecifier buffer)

    if BinaryUnit.byteSize romSize <> Array.length buffer then
      return! Error("ROM size specifier doesn't match actual length of ROM.")
    else
      return
        { mbc = mbcSpecifier buffer
          rom = Memory.rom buffer
          ram = Option.map Memory.ram ramSize }
  }


  let readRam address cartridge =
    cartridge.ram
      |> Option.map (Memory.get address)
      |> Option.defaultValue 0xffuy


  let writeRam address data cartridge =
    cartridge.ram
      |> Option.iter (Memory.put address data)


  let readRom address cartridge =
    cartridge.rom
      |> Memory.get address
