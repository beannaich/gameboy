namespace GameBoy.Boards

open GameBoy

type Board =
  | Board of Reader * Writer

module Board =

  let private createNROM cartridge =
    Board
      ( NROM.read cartridge
      , NROM.write cartridge )


  let private createMBC1 cartridge =
    let mbc1 = MBC1.init ()
    Board
      ( MBC1.read cartridge mbc1
      , MBC1.write cartridge mbc1 )


  let private createMBC2 cartridge =
    let mbc2 = MBC2.init ()
    Board
      ( MBC2.read cartridge mbc2
      , MBC2.write mbc2 )


  let private createMBC3 cartridge =
    let mbc3 = MBC3.init ()
    Board
      ( MBC3.read cartridge mbc3
      , MBC3.write cartridge mbc3 )


  let private createMBC5 cartridge =
    let mbc5 = MBC5.init ()
    Board
      ( MBC5.read cartridge mbc5
      , MBC5.write cartridge mbc5 )


  let create cartridge =
    match cartridge.mbc with
    | 0x00uy -> Ok <| createNROM(cartridge) // ROM ONLY
    | 0x01uy
    | 0x02uy
    | 0x03uy -> Ok <| createMBC1(cartridge)
    | 0x05uy
    | 0x06uy -> Ok <| createMBC2(cartridge)
    | 0x08uy -> Error "Unsupported: ROM+RAM"
    | 0x09uy -> Error "Unsupported: ROM+RAM+BATTERY"
    | 0x0buy -> Error "Unsupported: MMM01"
    | 0x0cuy -> Error "Unsupported: MMM01+RAM"
    | 0x0duy -> Error "Unsupported: MMM01+RAM+BATTERY"
    | 0x0fuy
    | 0x10uy
    | 0x11uy
    | 0x12uy
    | 0x13uy -> Ok <| createMBC3(cartridge)
    | 0x15uy -> Error "Unsupported: MBC4"
    | 0x16uy -> Error "Unsupported: MBC4+RAM"
    | 0x17uy -> Error "Unsupported: MBC4+RAM+BATTERY"
    | 0x19uy
    | 0x1auy
    | 0x1buy
    | 0x1cuy
    | 0x1duy
    | 0x1euy -> Ok <| createMBC5(cartridge)
    | 0xfcuy -> Error "Unsupported: POCKET CAMERA"
    | 0xfduy -> Error "Unsupported: BANDAI TAMA5"
    | 0xfeuy -> Error "Unsupported: HuC3"
    | 0xffuy -> Error "Unsupported: HuC1+RAM+BATTERY"
    | type' ->
      Error (sprintf "Unrecognized board type '$%02x'." type')


  let read (Board(reader, _)) = reader


  let write (Board(_, writer)) = writer
