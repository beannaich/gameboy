namespace GameBoy

open GameBoy.Platform.Input

type Pad =
  private
    { input: InputBackend

      mutable p14: bool
      mutable p15: bool
      mutable p14Latch: byte
      mutable p15Latch: byte }

module Pad =

  let init () =
    let input = InputBackend(0, 10)
    input.Map 0 "A"
    input.Map 1 "X"
    input.Map 2 "Back"
    input.Map 3 "Menu"
    input.Map 4 "DPad-R"
    input.Map 5 "DPad-L"
    input.Map 6 "DPad-U"
    input.Map 7 "DPad-D"
    input.Map 8 "B"
    input.Map 9 "Y"

    { input = input

      p14 = false
      p15 = false
      p14Latch = 0uy
      p15Latch = 0uy }


  let frame pad =
    pad.input.Update()

    pad.p15Latch <- 0xffuy ^^^ 0x20uy

    if (pad.input.Pressed 0) then pad.p15Latch <- pad.p15Latch ^^^ 0x1uy
    if (pad.input.Pressed 1) then pad.p15Latch <- pad.p15Latch ^^^ 0x2uy
    if (pad.input.Pressed 2) then pad.p15Latch <- pad.p15Latch ^^^ 0x4uy
    if (pad.input.Pressed 3) then pad.p15Latch <- pad.p15Latch ^^^ 0x8uy

    pad.p14Latch <- 0xffuy ^^^ 0x10uy

    if (pad.input.Pressed 4) then pad.p14Latch <- pad.p14Latch ^^^ 0x1uy
    if (pad.input.Pressed 5) then pad.p14Latch <- pad.p14Latch ^^^ 0x2uy
    if (pad.input.Pressed 6) then pad.p14Latch <- pad.p14Latch ^^^ 0x4uy
    if (pad.input.Pressed 7) then pad.p14Latch <- pad.p14Latch ^^^ 0x8uy
