namespace GameBoy.Processors.LR35902

type Core =
  { status: Status
    registers: Registers
    mutable cycles: int
    mutable code: uint8
    mutable ime: bool
    mutable halt: bool
    mutable stop: bool }

module Core =

  let init () =
    { status = Status.init ()
      registers = Registers.init ()
      cycles = 0
      code = 0uy
      ime = false
      halt = false
      stop = false }


  let zeroExtend (value: uint8) =
    value |> uint16


  let signExtend (value: uint8) =
    value |> int8 |> uint16


  let private tick core =
    core.cycles <- core.cycles + 4


  let private read reader address core =
    core |> tick
    reader address


  let private write writer address data core =
    core |> tick
    writer address data


  let inline private carryBits a b r =
    (a &&& b) ||| ((a ^^^ b) &&& ~~~r)


  let private flag core =
    match ((int core.code) >>> 3) &&& 3 with
    | 0 -> core.status.z = 0
    | 1 -> core.status.z = 1
    | 2 -> core.status.c = 0
    | 3 -> core.status.c = 1
    | _ ->
      false


  let private getOperand reader field core =
    match (int field) &&& 7 with
    | 0 -> core.registers.b
    | 1 -> core.registers.c
    | 2 -> core.registers.d
    | 3 -> core.registers.e
    | 4 -> core.registers.h
    | 5 -> core.registers.l
    | 6 -> core |> read reader (core.registers |> Registers.hl)
    | _ -> core.registers.a


  let private setOperand writer field data core =
    match (int field) &&& 7 with
    | 0 -> core.registers.b <- data
    | 1 -> core.registers.c <- data
    | 2 -> core.registers.d <- data
    | 3 -> core.registers.e <- data
    | 4 -> core.registers.h <- data
    | 5 -> core.registers.l <- data
    | 6 -> core |> write writer (core.registers |> Registers.hl) data
    | _ -> core.registers.a <- data


  let private fetch8 reader core =
    let data = core |> read reader (core.registers |> Registers.pc)
    core.registers |> Registers.mapPC (fun pc -> pc + 1us)
    data


  let private fetch16 reader core =
    let lo = core |> fetch8 reader
    let hi = core |> fetch8 reader
    Registers.makeWord hi lo


  let private pop8 reader core =
    let data = core |> read reader (core.registers |> Registers.sp)
    core.registers |> Registers.mapSP (fun sp -> sp + 1us)
    data


  let private pop16 reader core =
    let lo = core |> pop8 reader
    let hi = core |> pop8 reader
    Registers.makeWord hi lo


  let private push8 writer data core =
    core.registers |> Registers.mapSP (fun sp -> sp - 1us)
    core |> write writer (core.registers |> Registers.sp) data


  //#region Extended Opcodes


  let private bit data core =
    let shift = ((int core.code) >>> 3) &&& 7

    core.status.z <- if (data &&& (1uy <<< shift)) = 0uy then 1 else 0
    core.status.n <- 0
    core.status.h <- 1


  let private shl data carry core =
    core.status.c <- ((int data) >>> 7)

    let data = (data <<< 1) ||| (byte carry)

    core.status.z <- if data = 0uy then 1 else 0
    core.status.n <- 0
    core.status.h <- 0

    data


  let private shr data carry core =
    core.status.c <- ((int data) &&& 0x01)

    let data = (data >>> 1) ||| ((byte carry) <<< 7)

    core.status.z <- if data = 0uy then 1 else 0
    core.status.n <- 0
    core.status.h <- 0

    data


  let private res data core =
    let shift = ((int core.code) >>> 3) &&& 7
    let mask = 1uy <<< shift

    byte (data &&& ~~~mask)


  let private set data core =
    let shift = ((int core.code) >>> 3) &&& 7
    let mask = 1uy <<< shift

    byte (data ||| mask)


  let private swap data core =
    let data = (data >>> 4) ||| (data <<< 4)

    core.status.z <- if data = 0uy then 1 else 0
    core.status.n <- 0
    core.status.h <- 0
    core.status.c <- 0

    data


  let private rlc op core =
    core |> shl op ((int op) >>> 7)


  let private rrc op core =
    core |> shr op ((int op) &&& 1)


  let private rl op core =
    core |> shl op core.status.c


  let private rr op core =
    core |> shr op core.status.c


  let private sla op core =
    core |> shl op 0


  let private sra op core =
    core |> shr op ((int op) >>> 7)


  let private srl op core =
    core |> shr op 0


  let private extCode reader writer core =
    core.code <- fetch8 reader core

    let operand = core |> getOperand reader core.code

    match core.code with
    | n when n < 0x08uy -> (* rlc  *) core |> setOperand writer core.code (rlc operand core)
    | n when n < 0x10uy -> (* rrc  *) core |> setOperand writer core.code (rrc operand core)
    | n when n < 0x18uy -> (* rl   *) core |> setOperand writer core.code (rl operand core)
    | n when n < 0x20uy -> (* rr   *) core |> setOperand writer core.code (rr operand core)
    | n when n < 0x28uy -> (* sla  *) core |> setOperand writer core.code (sla operand core)
    | n when n < 0x30uy -> (* sra  *) core |> setOperand writer core.code (sra operand core)
    | n when n < 0x38uy -> (* swap *) core |> setOperand writer core.code (swap operand core)
    | n when n < 0x40uy -> (* srl  *) core |> setOperand writer core.code (srl operand core)
    | n when n < 0x80uy -> (* bit  *) core |> bit operand
    | n when n < 0xc0uy -> (* res  *) core |> setOperand writer core.code (res operand core)
    | _                 -> (* set  *) core |> setOperand writer core.code (set operand core)


  //#endregion


  //#region Standard Opcodes

  // -- 8-bit instructions --


  let private call reader writer core =
    let pc = core |> fetch16 reader

    if core.code = 0xcduy || (flag core) then
      core |> tick

      core |> push8 writer (core.registers |> Registers.pch)
      core |> push8 writer (core.registers |> Registers.pcl)

      core.registers |> Registers.withPC pc


  let private ccf core =
    core.status.n <- 0
    core.status.h <- 0
    core.status.c <- core.status.c ^^^ 1


  let private cpl core =
    core.registers.a <- ~~~core.registers.a
    core.status.n <- 1
    core.status.h <- 1


  let private daa core =
    if (core.status.n = 1) then
      if (core.status.c = 1) then core.registers.a <- core.registers.a - 0x60uy
      if (core.status.h = 1) then core.registers.a <- core.registers.a - 0x06uy
    else
      if (core.status.c = 1 || (core.registers.a &&& 0xffuy) > 0x99uy) then
        core.registers.a <- core.registers.a + 0x60uy
        core.status.c <- 1

      if (core.status.h = 1 || (core.registers.a &&& 0x0fuy) > 0x09uy) then
        core.registers.a <- core.registers.a + 0x06uy

    core.status.z <- if core.registers.a = 0uy then 1 else 0
    core.status.h <- 0


  let private jam core =
    failwithf "Invalid instruction $%02x" core.code


  let private jp reader core =
    let pc = core |> fetch16 reader

    if core.code = 0xc3uy || (flag core) then
      core |> tick

      core.registers |> Registers.withPC pc


  let private jr reader core =
    let data = core |> fetch8 reader |> signExtend

    if core.code = 0x18uy || (flag core) then
      core |> tick
      core.registers |> Registers.mapPC (fun pc -> pc + data)


  let private ld reader writer core =
    let data = core |> getOperand reader core.code
    core |> setOperand writer (core.code >>> 3) data


  let private ret reader core =
    if core.code = 0xc9uy || (flag core) then
      if core.code <> 0xc9uy then (tick core)
      core.registers |> Registers.withPC (core |> pop16 reader)

    core |> tick


  let private reti reader core =
    core.registers |> Registers.withPC (core |> pop16 reader)

    core |> tick

    core.ime <- true


  let private scf core =
    core.status.n <- 0
    core.status.h <- 0
    core.status.c <- 1


  let private and' data core =
    core.registers.a <- core.registers.a &&& data
    core.status.z <- if core.registers.a = 0uy then 1 else 0
    core.status.n <- 0
    core.status.h <- 1
    core.status.c <- 0


  let private cp data core =
    let temp = (core.registers.a + ~~~data + 1uy)
    let bits = ~~~(carryBits core.registers.a (~~~data) temp)

    core.status.z <- if temp = 0uy then 1 else 0
    core.status.n <- 1
    core.status.h <- ((int bits) >>> 3) &&& 1
    core.status.c <- ((int bits) >>> 7) &&& 1


  let private dec data core =
    let data = data - 1uy

    core.status.z <- if (data &&& 0xffuy) = 0x00uy then 1 else 0
    core.status.n <- 1
    core.status.h <- if (data &&& 0x0fuy) = 0x0fuy then 1 else 0

    data


  let private inc data core =
    let data = data + 1uy

    core.status.z <- if (data &&& 0xffuy) = 0x00uy then 1 else 0
    core.status.n <- 0
    core.status.h <- if (data &&& 0x0fuy) = 0x00uy then 1 else 0

    data


  let private or' data core =
    core.registers.a <- core.registers.a ||| data
    core.status.z <- if core.registers.a = 0uy then 1 else 0
    core.status.n <- 0
    core.status.h <- 0
    core.status.c <- 0


  let private rol carry core =
    core.status.z <- 0
    core.status.n <- 0
    core.status.h <- 0
    core.status.c <- ((int core.registers.a) >>> 7)

    core.registers.a <- (core.registers.a <<< 1) ||| (byte carry)


  let private ror carry core =
    core.status.z <- 0
    core.status.n <- 0
    core.status.h <- 0
    core.status.c <- ((int core.registers.a) &&& 0x01)

    core.registers.a <- (byte)((core.registers.a >>> 1) ||| ((byte carry) <<< 7))


  let private xor data core =
    core.registers.a <- core.registers.a ^^^ data
    core.status.z <- if core.registers.a = 0uy then 1 else 0
    core.status.n <- 0
    core.status.h <- 0
    core.status.c <- 0


  let private add data carry core =
    let temp = (core.registers.a + data) + (byte carry)
    let bits = carryBits core.registers.a data temp

    core.status.z <- if temp = 0uy then 1 else 0
    core.status.n <- 0
    core.status.h <- ((int bits) >>> 3) &&& 1
    core.status.c <- ((int bits) >>> 7) &&& 1

    core.registers.a <- temp


  let private sub data carry core =
    let temp = (core.registers.a - data) - (byte carry)
    let bits = ~~~(carryBits core.registers.a (~~~data) temp)

    core.status.z <- if temp = 0uy then 1 else 0
    core.status.n <- 1
    core.status.h <- ((int bits) >>> 3) &&& 1
    core.status.c <- ((int bits) >>> 7) &&& 1

    core.registers.a <- temp


  let rst writer addr core =
    core |> tick

    core |> push8 writer (core.registers |> Registers.pch)
    core |> push8 writer (core.registers |> Registers.pcl)

    core.registers |> Registers.withPC addr


  // -- 16-bit instructions --


  let private add16 data core =
    let hl = core.registers |> Registers.hl
    let temp = hl + data
    let bits = carryBits hl data temp

    core.status.n <- 0
    core.status.h <- ((int bits) >>> 11) &&& 1
    core.status.c <- ((int bits) >>> 15) &&& 1

    core.registers |> Registers.withHL temp

    core |> tick


  let private stdCode reader writer core =
    if core.halt || core.stop then
      core |> tick
    else
      core.code <- core |> fetch8 reader

      match int core.code with
      | 0x00 -> ()
      | 0x10 -> core.stop <- true
      | 0x76 -> core.halt <- true

      | 0xcb -> core |> extCode reader writer
      | 0xf3 -> core.ime <- false
      | 0xfb -> core.ime <- true

      | 0x07 -> core |> rol ((int core.registers.a) >>> 7)
      | 0x0f -> core |> ror ((int core.registers.a) &&& 1)
      | 0x17 -> core |> rol core.status.c
      | 0x1f -> core |> ror core.status.c
      | 0x27 -> core |> daa
      | 0x2f -> core |> cpl
      | 0x37 -> core |> scf
      | 0x3f -> core |> ccf

      | 0xcd | 0xc4 | 0xcc | 0xd4 | 0xdc -> core |> call reader writer
      | 0xc9 | 0xc0 | 0xc8 | 0xd0 | 0xd8 -> core |> ret reader
      | 0xc3 | 0xc2 | 0xca | 0xd2 | 0xda -> core |> jp reader
      | 0x18 | 0x20 | 0x28 | 0x30 | 0x38 -> core |> jr reader

      | 0x01 -> core.registers |> Registers.withBC (core |> fetch16 reader)
      | 0x11 -> core.registers |> Registers.withDE (core |> fetch16 reader)
      | 0x21 -> core.registers |> Registers.withHL (core |> fetch16 reader)
      | 0x31 -> core.registers |> Registers.withSP (core |> fetch16 reader)

      | 0x02 ->
        core |> write writer (core.registers |> Registers.bc) core.registers.a

      | 0x12 ->
        core |> write writer (core.registers |> Registers.de) core.registers.a

      | 0x22 ->
        core |> write writer (core.registers |> Registers.hl) core.registers.a
        core.registers |> Registers.mapHL (fun hl -> hl + 1us)

      | 0x32 ->
        core |> write writer (core.registers |> Registers.hl) core.registers.a
        core.registers |> Registers.mapHL (fun hl -> hl - 1us)

      | 0x0a ->
        core.registers.a <- core |> read reader (core.registers |> Registers.bc)

      | 0x1a ->
        core.registers.a <- core |> read reader (core.registers |> Registers.de)

      | 0x2a ->
        core.registers.a <- core |> read reader (core.registers |> Registers.hl)
        core.registers |> Registers.mapHL (fun hl -> hl + 1us)

      | 0x3a ->
        core.registers.a <- core |> read reader (core.registers |> Registers.hl)
        core.registers |> Registers.mapHL (fun hl -> hl - 1us)

      | 0xe0 -> core |> write writer (0xff00us + (zeroExtend <| fetch8 reader core)) core.registers.a
      | 0xe2 -> core |> write writer (0xff00us + (zeroExtend <| core.registers.c)) core.registers.a
      | 0xf0 -> core.registers.a <- core |> read reader (0xff00us + (zeroExtend <| fetch8 reader core))
      | 0xf2 -> core.registers.a <- core |> read reader (0xff00us + (zeroExtend <| core.registers.c))

      | 0xea -> // ld ($nnnn),a
        core.registers |> Registers.withEA (core |> fetch16 reader)

        core |> write writer (core.registers |> Registers.ea) core.registers.a

      | 0xfa -> // ld a,($nnnn)
        core.registers |> Registers.withEA (core |> fetch16 reader)

        core.registers.a <- core |> read reader (core.registers |> Registers.ea)

      | 0x08 -> // ld ($nnnn),sp
        core.registers |> Registers.withEA (core |> fetch16 reader)

        core |> write writer (core.registers |> Registers.ea) (core.registers |> Registers.spl)
        core.registers |> Registers.withEA ((core.registers |> Registers.ea) + 1us)
        core |> write writer (core.registers |> Registers.ea) (core.registers |> Registers.sph)

      | 0xd9 -> core |> reti reader

      | 0xe8 -> // add sp,sp,#$nn
        let data = core |> fetch8 reader |> signExtend
        let sp = core.registers |> Registers.sp
        let temp = sp + data
        let bits = carryBits sp data temp

        core.status.z <- 0
        core.status.n <- 0
        core.status.h <- ((int bits) >>> 3) &&& 1
        core.status.c <- ((int bits) >>> 7) &&& 1

        core.registers |> Registers.withSP temp

        core |> tick
        core |> tick

      | 0xf8 -> // add hl,sp,#$nn
        let data = core |> fetch8 reader |> signExtend
        let sp = core.registers |> Registers.sp
        let temp = sp + data
        let bits = carryBits sp data temp

        core.status.z <- 0
        core.status.n <- 0
        core.status.h <- ((int bits) >>> 3) &&& 1
        core.status.c <- ((int bits) >>> 7) &&& 1

        core.registers |> Registers.withHL temp

        core |> tick

      | 0xe9 ->
        core.registers |> Registers.withPC (core.registers |> Registers.hl) // ld pc,hl

      | 0xf9 ->
        core |> tick
        core.registers |> Registers.withSP (core.registers |> Registers.hl) // ld sp,hl

      | 0x03 ->
        core |> tick
        core.registers |> Registers.mapBC (fun bc -> bc + 1us)

      | 0x13 ->
        core |> tick
        core.registers |> Registers.mapDE (fun de -> de + 1us)

      | 0x23 ->
        core |> tick
        core.registers |> Registers.mapHL (fun hl -> hl + 1us)

      | 0x33 ->
        core |> tick
        core.registers |> Registers.mapSP (fun sp -> sp + 1us)

      | 0x0b ->
        core |> tick
        core.registers |> Registers.mapBC (fun bc -> bc - 1us)

      | 0x1b ->
        core |> tick
        core.registers |> Registers.mapDE (fun de -> de - 1us)

      | 0x2b ->
        core |> tick
        core.registers |> Registers.mapHL (fun hl -> hl - 1us)

      | 0x3b ->
        core |> tick
        core.registers |> Registers.mapSP (fun sp -> sp - 1us)

      | 0x09 -> core |> add16 (core.registers |> Registers.bc)
      | 0x19 -> core |> add16 (core.registers |> Registers.de)
      | 0x29 -> core |> add16 (core.registers |> Registers.hl)
      | 0x39 -> core |> add16 (core.registers |> Registers.sp)

      | 0x04 | 0x0c | 0x14 | 0x1c | 0x24 | 0x2c | 0x34 | 0x3c -> core |> setOperand writer (core.code >>> 3) (core |> inc (core |> getOperand reader (core.code >>> 3)))
      | 0x05 | 0x0d | 0x15 | 0x1d | 0x25 | 0x2d | 0x35 | 0x3d -> core |> setOperand writer (core.code >>> 3) (core |> dec (core |> getOperand reader (core.code >>> 3)))
      | 0x06 | 0x0e | 0x16 | 0x1e | 0x26 | 0x2e | 0x36 | 0x3e -> core |> setOperand writer (core.code >>> 3) (fetch8 reader core)
      | 0x40 | 0x41 | 0x42 | 0x43 | 0x44 | 0x45 | 0x46 | 0x47 -> core |> ld reader writer
      | 0x48 | 0x49 | 0x4a | 0x4b | 0x4c | 0x4d | 0x4e | 0x4f -> core |> ld reader writer
      | 0x50 | 0x51 | 0x52 | 0x53 | 0x54 | 0x55 | 0x56 | 0x57 -> core |> ld reader writer
      | 0x58 | 0x59 | 0x5a | 0x5b | 0x5c | 0x5d | 0x5e | 0x5f -> core |> ld reader writer
      | 0x60 | 0x61 | 0x62 | 0x63 | 0x64 | 0x65 | 0x66 | 0x67 -> core |> ld reader writer
      | 0x68 | 0x69 | 0x6a | 0x6b | 0x6c | 0x6d | 0x6e | 0x6f -> core |> ld reader writer
      | 0x70 | 0x71 | 0x72 | 0x73 | 0x74 | 0x75 | (*76*) 0x77 -> core |> ld reader writer
      | 0x78 | 0x79 | 0x7a | 0x7b | 0x7c | 0x7d | 0x7e | 0x7f -> core |> ld reader writer
      | 0x80 | 0x81 | 0x82 | 0x83 | 0x84 | 0x85 | 0x86 | 0x87 -> core |> add (core |> getOperand reader core.code) 0
      | 0x88 | 0x89 | 0x8a | 0x8b | 0x8c | 0x8d | 0x8e | 0x8f -> core |> add (core |> getOperand reader core.code) core.status.c
      | 0x90 | 0x91 | 0x92 | 0x93 | 0x94 | 0x95 | 0x96 | 0x97 -> core |> sub (core |> getOperand reader core.code) 0
      | 0x98 | 0x99 | 0x9a | 0x9b | 0x9c | 0x9d | 0x9e | 0x9f -> core |> sub (core |> getOperand reader core.code) core.status.c
      | 0xa0 | 0xa1 | 0xa2 | 0xa3 | 0xa4 | 0xa5 | 0xa6 | 0xa7 -> core |> and' (core |> getOperand reader core.code)
      | 0xa8 | 0xa9 | 0xaa | 0xab | 0xac | 0xad | 0xae | 0xaf -> core |> xor (core |> getOperand reader core.code)
      | 0xb0 | 0xb1 | 0xb2 | 0xb3 | 0xb4 | 0xb5 | 0xb6 | 0xb7 -> core |> or' (core |> getOperand reader core.code)
      | 0xb8 | 0xb9 | 0xba | 0xbb | 0xbc | 0xbd | 0xbe | 0xbf -> core |> cp (core |> getOperand reader core.code)
      | 0xc7 -> core |> rst writer 0x0000us
      | 0xcf -> core |> rst writer 0x0008us
      | 0xd7 -> core |> rst writer 0x0010us
      | 0xdf -> core |> rst writer 0x0018us
      | 0xe7 -> core |> rst writer 0x0020us
      | 0xef -> core |> rst writer 0x0028us
      | 0xf7 -> core |> rst writer 0x0030us
      | 0xff -> core |> rst writer 0x0038us

      | 0xc1 ->
        core.registers |> Registers.withBC (core |> pop16 reader)

      | 0xd1 ->
        core.registers |> Registers.withDE (core |> pop16 reader)

      | 0xe1 ->
        core.registers |> Registers.withHL (core |> pop16 reader)

      | 0xf1 ->
        core.registers |> Registers.withAF (core |> pop16 reader)
        core.status |> Status.load core.registers.f

      | 0xc5 ->
        core |> tick
        core |> push8 writer core.registers.b
        core |> push8 writer core.registers.c

      | 0xd5 ->
        core |> tick
        core |> push8 writer core.registers.d
        core |> push8 writer core.registers.e

      | 0xe5 ->
        core |> tick
        core |> push8 writer core.registers.h
        core |> push8 writer core.registers.l

      | 0xf5 ->
        core |> tick
        core |> push8 writer core.registers.a
        core |> push8 writer core.registers.f

      | 0xc6 -> core |> add (fetch8 reader core) 0
      | 0xce -> core |> add (fetch8 reader core) core.status.c
      | 0xd6 -> core |> sub (fetch8 reader core) 0
      | 0xde -> core |> sub (fetch8 reader core) core.status.c
      | 0xe6 -> core |> and' (fetch8 reader core)
      | 0xee -> core |> xor (fetch8 reader core)
      | 0xf6 -> core |> or' (fetch8 reader core)
      | 0xfe -> core |> cp (fetch8 reader core)

      | 0xd3
      | 0xdb
      | 0xdd
      | 0xe3
      | 0xe4
      | 0xeb
      | 0xec
      | 0xed
      | 0xf4
      | 0xfc
      | 0xfd -> core |> jam

      | _ ->
        ()


  //#endregion


  let step reader writer core =
    core.cycles <- 0
    core |> stdCode reader writer
    core.cycles
