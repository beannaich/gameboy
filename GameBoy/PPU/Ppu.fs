namespace GameBoy.PPU

type Ppu =
  { mutable bkgEnabled: bool
    mutable lcdEnabled: bool
    mutable objEnabled: bool
    mutable wndEnabled: bool
    mutable bkgPalette: int
    mutable objPalette: int[] // = new byte[2]
    mutable scrollX: int
    mutable scrollY: int
    mutable windowX: int
    mutable windowY: int
    mutable bkgCharAddress: int // = 0x1000
    mutable bkgNameAddress: int // = 0x1800
    mutable wndNameAddress: int // = 0x1800
    mutable objRasters: int // = 8
    mutable control: int
    mutable h: int
    mutable v: int
    mutable vCheck: int
    mutable ff40: byte

    mutable dmaTrigger: bool
    mutable dmaSegment: byte }

module Ppu =

  open GameBoy.CPU
  open GameBoy.Platform.Util
  open GameBoy.Platform.Video


  let [<Literal>] PriorityClr = 0
  let [<Literal>] PriorityBkg = 1
  let [<Literal>] PrioritySpr = 2

  let [<Literal>] SpriteSeq = 2
  let [<Literal>] ActiveSeq = 3
  let [<Literal>] HBlankSeq = 0
  let [<Literal>] VBlankSeq = 1

  let [<Literal>] HBlankInt = 0x08
  let [<Literal>] VBlankInt = 0x10
  let [<Literal>] SpriteInt = 0x20
  let [<Literal>] VCheckInt = 0x40


  let private fromRGB r g b =
    0xff000000
      ||| (r <<< 16)
      ||| (g <<<  8)
      ||| (b <<<  0)


  let shadeTable = [|
    fromRGB 224 248 208
    fromRGB 136 192 112
    fromRGB  48 104  80
    fromRGB   8  24  32
  |]


  let priority = Array.zeroCreate<int> 160


  let init () =
    { bkgEnabled = false
      lcdEnabled = false
      objEnabled = false
      wndEnabled = false
      bkgPalette = 0
      objPalette = Array.zeroCreate<int> 2
      scrollX = 0
      scrollY = 0
      windowX = 0
      windowY = 0
      bkgCharAddress = 0x1000
      bkgNameAddress = 0x1800
      wndNameAddress = 0x1800
      objRasters = 8
      control = 0
      h = 0
      v = 0
      vCheck = 0
      ff40 = 0uy
      dmaTrigger = false
      dmaSegment = 0uy }


  let changeSequence irq ppu sequence =
    if sequence = (ppu.control &&& 3) then
      ()
    else
      if ((sequence = HBlankSeq && (ppu.control &&& HBlankInt) <> 0) ||
          (sequence = VBlankSeq && (ppu.control &&& VBlankInt) <> 0) ||
          (sequence = SpriteSeq && (ppu.control &&& SpriteInt) <> 0)) then
        irq Interrupt.Status

      ppu.control <- ppu.control &&& (~~~3)
      ppu.control <- ppu.control ||| sequence


  let updateSequence irq ppu =
    if ppu.lcdEnabled then
      if ppu.v < 144 then
        match ppu.h with
        | h when h <  80 -> changeSequence irq ppu SpriteSeq
        | h when h < 252 -> changeSequence irq ppu ActiveSeq
        | h when h < 456 -> changeSequence irq ppu HBlankSeq
        | _ ->
          ()
      else
        changeSequence irq ppu VBlankSeq


  let getShade palette color =
    shadeTable.[(palette >>> (color * 2)) &&& 3]


  let renderBkg video ppu (read: uint16 -> byte) =
    if not ppu.bkgEnabled then
      ()
    else
      let xPos = (ppu.scrollX) &&& 0xff
      let yPos = (ppu.scrollY + ppu.v) &&& 0xff
      let mutable fine = (ppu.scrollX) &&& 7

      let mutable ntaddr = ppu.bkgNameAddress ||| ((yPos &&& ~~~7) <<< 2) ||| ((xPos &&& ~~~7) >>> 3)
      let mutable px = 0

      for tx in 0 .. 20 do
        let name = int <| read (uint16 ntaddr)
        let chaddr =
          if (ppu.bkgCharAddress = 0x9000 && (name &&& 0x80) = 0) then
            0x9000 ||| (name <<< 4) ||| ((yPos &&& 7) <<< 1)
          else
            0x8000 ||| (name <<< 4) ||| ((yPos &&& 7) <<< 1)

        let palette = ppu.bkgPalette
        let mutable bit0 = int <| read(uint16 (chaddr ||| 0))
        let mutable bit1 = int <| read(uint16 (chaddr ||| 1))

        bit0 <- bit0 <<< fine
        bit1 <- bit1 <<< fine

        for x in 0 .. 7 do
          let color = ((bit0 &&& 0x80) >>> 7) ||| ((bit1 &&& 0x80) >>> 6)
          bit0 <- bit0 <<< 1
          bit1 <- bit1 <<< 1

          if px < 160 then
            priority.[px] <- if (color <> 0) then PriorityBkg else PriorityClr
            VideoSink.run video px ppu.v (getShade palette color)
            px <- px + 1

        fine <- 0

        ntaddr <- (ntaddr &&& 0xffe0) ||| ((ntaddr + 1) &&& 0x1f)


  let renderWnd video ppu (read: uint16 -> byte) =
    if not ppu.wndEnabled || ppu.v < ppu.windowY then
      ()
    else
      let mutable x = (ppu.windowX - 7)
      let y = (ppu.v - ppu.windowY) &&& 0xff

      let mutable nameAddress = ppu.wndNameAddress ||| ((y &&& ~~~7) <<< 2)

      let tx = (168 - x) / 8

      for i in 1 .. tx do
        let name = int <| read(uint16 (nameAddress))
        let charAddress =
          if ppu.bkgCharAddress = 0x9000 && name < 0x80 then
            0x9000 ||| (name <<< 4) ||| ((y &&& 7) <<< 1)
          else
            0x8000 ||| (name <<< 4) ||| ((y &&& 7) <<< 1)

        let palette = ppu.bkgPalette
        let mutable bit0 = int <| read(uint16 (charAddress ||| 0))
        let mutable bit1 = int <| read(uint16 (charAddress ||| 1))

        for j in 0 .. 7 do
          let color = ((bit0 &&& 0x80) >>> 7) ||| ((bit1 &&& 0x80) >>> 6)
          bit0 <- bit0 <<< 1
          bit1 <- bit1 <<< 1

          if x >= 0 && x < 160 then
            priority.[x] <- PriorityBkg
            VideoSink.run video x ppu.v (getShade palette color)

          x <- x + 1

        nameAddress <- (nameAddress &&& 0xffe0) ||| ((nameAddress + 1) &&& 0x1f)


  let renderObj video ppu (read: uint16 -> byte) =
    let mutable count = 0

    for i in 0 .. 4 .. 159 do
      if count < 0 then
        let y = read(uint16 (0xfe00 + i + 0))
        let x = read(uint16 (0xfe00 + i + 1))

        let yPos = (int y) - 16
        let mutable xPos = (int x) - 8
        let mutable name = int <| read(uint16 (0xfe00 + i + 2))
        let attr = int <| read(uint16 (0xfe00 + i + 3))

        let mutable line = (ppu.v - yPos) &&& 0xffff
        if line < ppu.objRasters then
            count <- count + 1

            if (attr &&& 0x40) <> 0 then
              line <- line ^^^ 0x0f

            if ppu.objRasters = 16 then
              name <- name &&& 0xfe

              if line >= 8 then
                name <- name ||| 0x01

            let addr = 0x8000 ||| (name <<< 4) ||| ((line <<< 1) &&& 0x000e)
            let mutable bit0 = int <| read (uint16 (addr ||| 0))
            let mutable bit1 = int <| read (uint16 (addr ||| 1))

            if (attr &&& 0x20) <> 0 then
              bit0 <- int <| Byte.reverse(bit0)
              bit1 <- int <| Byte.reverse(bit1)

            let palette = ppu.objPalette.[(attr >>> 4) &&& 1]

            for x in 0 .. 7 do
              if (xPos < 160) then
                if (xPos < 0 || priority.[xPos] = PrioritySpr) then
                  ()
                else
                  let color = ((bit0 >>> 7) &&& 0x1) ||| ((bit1 >>> 6) &&& 0x2)

                  if color <> 0 then
                    if (attr &&& 0x80) <> 0 then
                      if priority.[xPos] = PriorityClr then
                        priority.[xPos] <- PrioritySpr
                        VideoSink.run video xPos ppu.v (getShade palette color)
                    else
                      priority.[xPos] <- PrioritySpr
                      VideoSink.run video xPos ppu.v (getShade palette color)

              xPos <- xPos + 1
              bit0 <- bit0 <<< 1
              bit1 <- bit1 <<< 1


  let renderScanline video ppu read =
    if ppu.lcdEnabled then
      if ppu.bkgEnabled then
        renderBkg video ppu read

      if ppu.wndEnabled then
        renderWnd video ppu read

      if ppu.objEnabled then
        renderObj video ppu read


  let vCompare irq ppu =
    if ppu.v = ppu.vCheck then
      ppu.control <- ppu.control ||| 4

      if (ppu.control &&& VCheckInt) <> 0 then
        irq Interrupt.Status
    else
      ppu.control <- ppu.control &&& (~~~4)


  let tick video irq ppu read (write: uint16 -> byte -> unit) =
    if ppu.dmaTrigger then
      ppu.dmaTrigger <- false

      let mutable dstAddress = 0xfe00us
      let mutable srcAddress = (uint16 ppu.dmaSegment) <<< 8

      for i in 1 .. 160 do
        let data = read srcAddress
        write dstAddress data

        dstAddress <- dstAddress + 1us
        srcAddress <- srcAddress + 1us

    ppu.h <- ppu.h + 1

    if ppu.h = 252 && ppu.v < 144 then
      if ppu.lcdEnabled then
        renderScanline video ppu read
      else
        for i in 1 .. 160 do
          VideoSink.run video i ppu.v shadeTable.[0]

    if ppu.lcdEnabled then
      updateSequence irq ppu

    if ppu.h = 456 then
      ppu.h <- 0
      ppu.v <- ppu.v + 1

      if ppu.v = 154 then
        ppu.v <- 0

      if ppu.lcdEnabled then
        if ppu.v = 144 then
          irq Interrupt.VBlank

        vCompare irq ppu


  let clock irq read write video amount ppu =
    for i in 1 .. amount do
      tick video irq ppu read write
