module GameBoy.MemoryMap

open GameBoy
open GameBoy.APU
open GameBoy.Boards
open GameBoy.Platform


let readMMIO state address =
  match int address with
  | n when n >= 0xff10 && n <= 0xff2f -> Reader.run (Apu.read state.apu) address
  | n when n >= 0xff30 && n <= 0xff3f -> state.wave |> Memory.get (int address)
  | n when n >= 0xff80 && n <= 0xfffe -> state.hram |> Memory.get (int address)
  | 0xff00 ->
    if state.pad.p15 then
      state.pad.p15Latch
    elif state.pad.p14 then
      state.pad.p14Latch
    else
      0xffuy

  | 0xff04 -> byte state.tma.divider
  | 0xff05 -> byte state.tma.counter
  | 0xff06 -> byte state.tma.modulus
  | 0xff07 -> byte state.tma.control

  | 0xff0f -> state.cpu.irf

  | 0xff40 -> state.ppu.ff40
  | 0xff41 -> byte (0x80 ||| state.ppu.control)
  | 0xff42 -> byte state.ppu.scrollY
  | 0xff43 -> byte state.ppu.scrollX
  | 0xff44 -> byte state.ppu.v
  | 0xff45 -> byte state.ppu.vCheck
  | 0xff46 -> state.ppu.dmaSegment
  | 0xff47 -> byte state.ppu.bkgPalette
  | 0xff48 -> byte state.ppu.objPalette.[0]
  | 0xff49 -> byte state.ppu.objPalette.[1]
  | 0xff4a -> byte state.ppu.windowY
  | 0xff4b -> byte state.ppu.windowX

  | 0xffff -> state.cpu.ief

  | _ ->
    0xffuy


let writeMMIO resetDivider state address data =
  match int address with
  | n when n >= 0xff10 && n <= 0xff2f -> Writer.run (Apu.write state.apu) address data
  | n when n >= 0xff30 && n <= 0xff3f -> state.wave |> Memory.put (int address) data
  | n when n >= 0xff80 && n <= 0xfffe -> state.hram |> Memory.put (int address) data
  | 0xff00 ->
    state.pad.p15 <- (data &&& 0x20uy) = 0uy
    state.pad.p14 <- (data &&& 0x10uy) = 0uy

  | 0xff04 -> resetDivider()
  | 0xff05 -> state.tma.counter <- int data
  | 0xff06 -> state.tma.modulus <- int data
  | 0xff07 -> state.tma.control <- int data

  | 0xff0f -> state.cpu.irf <- byte data

  // ppu
  | 0xff40 ->
    if not state.ppu.lcdEnabled && (data &&& 0x80uy) <> 0uy then
      // lcd turning on
      state.ppu.h <- 4
      state.ppu.v <- 0

    state.ppu.ff40 <- byte data

    state.ppu.lcdEnabled     <- (data &&& 0x80uy) <> 0uy
    state.ppu.wndNameAddress <- if (data &&& 0x40uy) <> 0uy then 0x9c00 else 0x9800
    state.ppu.wndEnabled     <- (data &&& 0x20uy) <> 0uy
    state.ppu.bkgCharAddress <- if (data &&& 0x10uy) <> 0uy then 0x8000 else 0x9000
    state.ppu.bkgNameAddress <- if (data &&& 0x08uy) <> 0uy then 0x9c00 else 0x9800
    state.ppu.objRasters     <- if (data &&& 0x04uy) <> 0uy then 16 else 8
    state.ppu.objEnabled     <- (data &&& 0x02uy) <> 0uy
    state.ppu.bkgEnabled     <- (data &&& 0x01uy) <> 0uy

  | 0xff41 ->
    state.ppu.control <- state.ppu.control &&& ~~~0x87
    state.ppu.control <- state.ppu.control ||| ((int data) &&& 0x78)

  | 0xff42 -> state.ppu.scrollY <- int data
  | 0xff43 -> state.ppu.scrollX <- int data
  | 0xff44 ->
    state.ppu.h <- 0
    state.ppu.v <- 0

  | 0xff45 -> state.ppu.vCheck <- int data
  | 0xff46 ->
    state.ppu.dmaTrigger <- true
    state.ppu.dmaSegment <- byte data

  | 0xff47 -> state.ppu.bkgPalette <- int data
  | 0xff48 -> state.ppu.objPalette.[0] <- int data
  | 0xff49 -> state.ppu.objPalette.[1] <- int data
  | 0xff4a -> state.ppu.windowY <- int data
  | 0xff4b -> state.ppu.windowX <- int data

  | 0xff50 -> state.bootRomEnabled <- false

  | 0xffff -> state.cpu.ief <- byte data

  | _ ->
    ()


let read board state (address: uint16) =
  match int address with
  | n when n >= 0x0000 && n <= 0x00ff && state.bootRomEnabled -> state.bios |> Memory.get n
  | n when n >= 0x0000 && n <= 0x7fff -> Reader.run (Board.read board) address
  | n when n >= 0x8000 && n <= 0x9fff -> state.vram |> Memory.get (int address)
  | n when n >= 0xa000 && n <= 0xbfff -> Reader.run (Board.read board) address
  | n when n >= 0xc000 && n <= 0xfdff -> state.wram |> Memory.get (int address)
  | n when n >= 0xfe00 && n <= 0xfe9f -> state.oam  |> Memory.get (int address)
  | n when n >= 0xff00 && n <= 0xffff -> readMMIO state address
  | _ ->
    0xffuy


let write resetDivider board state (address: uint16) (data: byte) =
  match int address with
  | n when n >= 0x0000 && n <= 0x7fff -> Writer.run (Board.write board) address data
  | n when n >= 0x8000 && n <= 0x9fff -> state.vram |> Memory.put (int address) data
  | n when n >= 0xa000 && n <= 0xbfff -> Writer.run (Board.write board) address data
  | n when n >= 0xc000 && n <= 0xfdff -> state.wram |> Memory.put (int address) data
  | n when n >= 0xfe00 && n <= 0xfe9f -> state.oam  |> Memory.put (int address) data
  | n when n >= 0xff00 && n <= 0xffff -> writeMMIO resetDivider state address data
  | _ ->
    ()
